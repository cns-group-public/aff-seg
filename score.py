import argparse
import time
import os

from ava.core.metrics import IoUMetric, MAPMetric
from torch.utils.data import DataLoader
from common import get_dataset_type, initialize_model, load_pretrained_model, collate, CONFIG


def score(model_name, dataset_name, batch_size):

    dataset_type, dataset_args, other_args = get_dataset_type(dataset_name)

    print('Initialize dataset {} with arguments\n{}'.format(dataset_name, '\n'.join('  {}: {}'.format(k, v) for k,v in dataset_args.items())))
    d_test = dataset_type('test', None, **dataset_args)

    if os.path.isfile(model_name):

        import torch
        model_file = torch.load(model_name)
        weights = model_file['state_dict']
        model_name = model_file['model']
        model_args = model_file['arguments'][-1]
        model_args['transfer_mode'] = False
        model_args['pretrained'] = False
        model = initialize_model(model_name, cli_args={}, model_config=model_args)
        model.load_state_dict(weights, strict=True)
        # model = load_pretrained_model(model_name)
    else:
        model = initialize_model(model_name, cli_args=other_args, model_config=d_test.model_config)

    model.eval()

    loader = DataLoader(d_test, batch_size=batch_size, num_workers=2, collate_fn=collate)

    print('Score with batch size {} on {} samples'.format(batch_size, len(d_test)))

    time_start = time.time()

    metrics = [m() for m in model.metrics()][:2]
    print(metrics)

    for i, (vars_x, vars_y) in enumerate(loader):

        if i % 30 == 29:
            for j in range(len(metrics)):
                score = metrics[j].value()
                for name, s in zip(metrics[j].names(), score):
                    print(name, s)

        vars_x = [v.cuda() if v is not None else None for v in vars_x]
        vars_y = [v.cuda() if v is not None else None for v in vars_y]

        pred = model(*vars_x)

        for j in range(len(metrics)):
            metrics[j].add(pred, vars_y)

    print('\n\nFinal scores:')

    for i in range(len(metrics)):
        score = metrics[i].value()
        for name, s in zip(metrics[i].names(), score):
            print(name, s)

    print('took {:.3f}s'.format(time.time() - time_start))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model_name', help='model name')
    parser.add_argument('dataset_name', help='dataset name')
    parser.add_argument('--n', type=int, default=10, help='Number of episodes')
    parser.add_argument('--batch-size', type=int, default=10, help='Batch size')
    parser.add_argument('--precache', default=False, action='store_true')

    args, unknown_args = parser.parse_known_args()

    score(args.model_name, args.dataset_name, batch_size=1)

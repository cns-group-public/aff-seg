from functools import partial

import time
import torch
from torch import nn
import yaml
import numpy as np
from ade import ADEAff12, AffHQ12, OregonAffordances, AffSynth12
from ava.core.logging import *
from ava.models.dense.dense_resnet import ResNet50Dense
from ava.models.dense.pspnet import PSPNet

CONFIG = yaml.load(open('config.yaml'))

MODEL_INTERFACE = {
    'methods':  {
        '__init__': ((), (), True),
        'forward': (1, (), True),
        #'prepare_sample': (None, (), True),
        'metrics': (None, (), True),
        'name': (None, (), True),
        'normalize': (None, (), False),
        'loss': (('y_pred', 'y_gt'), (), True),
        'score': (('y_pred', 'y_gt'), (), True),
        'accumulative_scores': (None, (), False),
        'score_accumulated': (None, (), False),
        'get_default': (None, (), False),
    },
    'attributes': {
        'training_details': (dict, True),
        'training_state': (dict, True),
        'metric_names': (tuple, True),
        'acc_metric_names': (tuple, True),
        'prediction_names': (str, False),  # must be provided to know how many predictions are expected
        # optional
        'visualization_modes': (tuple, False),
        '_defaults': (tuple, True),
        'is_baseline': (bool, True),
    }
}

DATASET_INTERFACE = {
    'methods': {
        # name: required_arguments, optional_arguments, method_required
        # required
        '__init__': (('subset', 'max_samples'), (), True),
        '__len__': ((), (), True),
        # 'generator': (('batch_size',), ('start', 'max_samples', 'shuffle_samples', 'infinite'), True),
        # optional
        '__getitem__': (('index',), (), False),
    },
    # these must be defined after __init__
    'attributes': {
        # name: (type, attribute required)
        # required
        'chunk_names': (tuple, True),
        'sample_ids': (tuple, True),
        # optional
        'chunk_hooks': (dict, False),
        'model_config': (dict, False),
        'slice_labels': (tuple, False),
    }
}


def get_dataset_type(dataset_name):

    all_datasets = {'ADEAff12': ADEAff12, 'AffHQ12': AffHQ12, 'Oregon': OregonAffordances,
                    'Oregon-OWN': OregonAffordances, 'AffSynth12': AffSynth12}
    add_args = {'Oregon-OWN': {'split': 'own'}, 'Oregon': {'split': 'nyu'}}

    if dataset_name in all_datasets:
        dataset_type = all_datasets[dataset_name]
        additional_args = add_args[dataset_name] if dataset_name in add_args else {}
    else:
        raise FileNotFoundError('Dataset not found: {}'.format(dataset_name))

    additional_args = additional_args if additional_args is not None else {}
    dataset_default_args = DATASET_INTERFACE['methods']['__init__'][0] + DATASET_INTERFACE['methods']['__init__'][1]

    # TODO: ignore the arguments of train (this function).
    dataset_args = {k: v for k, v in additional_args.items()
                    if k in dataset_type.__init__.__code__.co_varnames and k not in dataset_default_args}

    other_args = {k: v for k, v in additional_args.items()
                    if k not in dataset_type.__init__.__code__.co_varnames and k not in dataset_default_args}

    return dataset_type, dataset_args, other_args


def collate_element(batch, cuda=False):

    if batch is None:
        out = None
    elif type(batch) == list and batch[0] is None:
        out = None
    else:
        if type(batch[0]) == np.ndarray:
            # print(np.array(batch).dtype)
            # xx
            # print(np.array(batch).shape)
            out = torch.from_numpy(np.array(batch))
        elif type(batch[0]) in {str, np.str_}:
            out = str(batch[0])
        elif type(batch[0]) == float:
            out = torch.FloatTensor(batch)
        elif type(batch[0]) in {int, np.int64}:
            out = torch.LongTensor(batch)
        else:
            raise ValueError('Failed to convert data into tensor. Type: {}, Length: {}, Element types: {}'.format(
                type(batch), len(batch), [type(b) for b in batch]
            ))


    # if out is not None:
    #     print(type(out))
    #     out.pin_memory()

    if cuda and hasattr(out, 'cuda'):
        out = out.cuda()


    return out


def collate(batch, cuda=False):
    variables_x = [collate_element([batch[i][0][j] for i in range(len(batch))], cuda=cuda) for j in range(len(batch[0][0]))]
    variables_y = [collate_element([batch[i][1][j] for i in range(len(batch))], cuda=cuda) for j in range(len(batch[0][1]))]

    return variables_x, variables_y


def monkey_patch_model(model, INTERFACE, sync_bn=False, multi_gpu=False):

    if sync_bn:
        from third_party.sync_bn.sync_batchnorm import DataParallelWithCallback
        DataParallel = partial(DataParallelWithCallback, device_ids=[0,1,2,3])
    elif multi_gpu:
        DataParallel = torch.nn.DataParallel
    else:
        DataParallel = lambda x: x

    original_model = model
    model = DataParallel(model)

    # this monkey-patching is ugly, but works
    for m in list(INTERFACE['methods'].keys()) + list(INTERFACE['attributes'].keys()):
        if hasattr(original_model, m) and m != 'forward':
            setattr(model, m, getattr(original_model, m))

    # TODO: introduce name attribute for models
    model.__class__.__name__ = original_model.__class__.__name__
    return model


def load_pretrained_model(model_name: str, cli_args=None, model_config=None,
                          no_weights=False,
                          multi_gpu=False, sync_bn=False):

    if cli_args is None:
        cli_args = {}

    if type(model_name) == dict:
        model_file = model_name
        model_name = model_file['model']
    else:
        log_important('\nload pre-trained model', model_name)
        model_file = torch.load(model_name)

    print('model created', model_file['creation_time'])
    loaded_model_name = model_file['model']

    try:
        from ava import models
        model_type = getattr(models, loaded_model_name)
    except ImportError:
        raise ImportError('Model not found. Are you sure it is imported in models/__init__.py?')

    # the model priorizes arguments by these sources in descending order: CLI, model_file, dataset.model_config
    model_args = model_config if model_config is not None else dict()

    # arguments of the last training round
    model_checkpoint_args = model_file['arguments'][-1]
    log_info('model file args', model_checkpoint_args)
    for k in model_checkpoint_args:
        if k in model_args and model_args[k] != model_checkpoint_args[k]:
            log_warning('Model parameter {} is overwritten by stored model configuration'.format(k))
        model_args[k] = model_checkpoint_args[k]

    # model_cli_args = {k: v for k, v in cli_args.items() if k in inspect.getargs(model_type.__init__.__code__).args}
    # cli_args = cli_args

    log_info('model cli args', cli_args)
    for k in cli_args:
        if k in model_args and model_args[k] != cli_args[k]:
            log_warning('Model argument {} is overwritten by CLI arguments'.format(k))
        model_args[k] = cli_args[k]

    # are there parameters missing?
    non_match_args = [k for k, v in model_checkpoint_args.items() if k not in model_args]
    msg = "These arguments required by {} were not provided: {}".format(model_name, ', '.join(non_match_args))
    assert len(non_match_args) == 0, msg

    if 'transfer_mode' in model_args:
        model_args['transfer_mode'] = False

    # model = model_type(**model_args)
    model = model_type(**model_args)
    model_name = model.name()
    model_class = model.__class__.__name__

    # load weights
    if not no_weights:
        log_important('Set pre-loaded weights for ', model_name)
        weights = model_file['state_dict']
        #log_info('Included submodules', set(['.'.join(k.split('.')[:2]) for k in weights]))

        # for some reason weight keys often start with "module.". This is fixed here:
        #if all([k.startswith('module') for k in weights]):
        #    weights = {k[7:]: v for k, v in weights.items()}

        model.load_state_dict(weights, strict=True)

    #model = monkey_patch_model(model, MODEL_INTERFACE, sync_bn, multi_gpu)

    # compatibility mode
    if 'dataset_arguments' not in model_file: model_file['dataset_arguments'] = [{}]

    model.training_details = { # 'checkpoint_filename': checkpoint_filename,
                              'model': model_class,
                              'model_name': model_name,
                              'arguments': model_file['arguments'] + [model_args],
                              'dataset': model_file['dataset'],
                              'dataset_arguments': model_file['dataset_arguments'],
                              'creation_time': model_file['creation_time'] + [time.strftime('%d.%m.%y %H:%M')],
                              'data_parallel': type(model) == nn.DataParallel,
                              }

    model.training_state = {'val_loss': model_file['val_loss'] + [], 'train_loss': model_file['val_loss'] + [],
                            'episode': model_file['episode'] + [], 'metrics': model_file['metrics'] + []}

    if CONFIG['CUDA']:
        model.cuda()

    return model



def initialize_model(model_name: str, cli_args=None, model_config=None, no_weights=False,
               multi_gpu=False, sync_bn=False):

    all_models = {'ResNet50Dense': ResNet50Dense, 'PSPNet': PSPNet}

    model_type = all_models[model_name]

    # the model priorizes arguments by these sources in descending order: CLI, dataset.model_config
    model_args = dict()

    if model_config is not None:
        model_args.update(model_config)
        log_warning('Extra model arguments (e.g. by dataset): {}'.format(', '.join(model_config.keys())))

    # if dataset_object is not None and hasattr(dataset_object, 'model_config') and not ignore_data_model_config:
    #     model_args.update(dataset_object.model_config)
    #     log_warning('Arguments provided by the dataset: {}'.format(', '.join(dataset_object.model_config.keys())))

    #model_cli_args = {k: v for k, v in cli_args.items() if k in model_type.__init__.__code__.co_varnames}
    #log_info('model cli args', model_cli_args)

    overwritten_args = {k: cli_args[k] for k in model_args if k in cli_args}
    if len(overwritten_args) > 0:
        log_warning('These arguments are overwritten by CLI:', ', '.join(overwritten_args))

    for k in cli_args:
        model_args[k] = cli_args[k]

    info_txt = ('Initialize model {}\n'
                '   model arguments:\n      {}\n'
                '   data parallel: {}')

    log_info(info_txt.format(model_name,
                             '\n      '.join('{}: {}'.format(k, v) for k, v in model_args.items()),
                             CONFIG['MULTI_GPU']))

    try:
        model = model_type(**model_args)
    except TypeError as err:
        raise TypeError('Error while initializing the model {}. You might need to provide more arguments. This can '
                        'be done using the model_config dictionary of the dataset or via the CLI. '
                        'Error message: {}'.format(model_type.__name__, err))

    log_important(torch.cuda.device_count(), 'available GPUs', 'using multi gpu' if multi_gpu else 'no multi gpu')

    model_class = model.__class__.__name__
    model_name = model.name()
    model = monkey_patch_model(model, MODEL_INTERFACE, sync_bn, multi_gpu)

    model.training_details = { # 'checkpoint_filename': checkpoint_filename,
                              'model': model_class,
                              'model_name': model_name,
                              'arguments': [model_args],
                              'dataset': [],
                              'dataset_arguments': [],
                              'creation_time': [time.strftime('%d.%m.%y %H:%M')]}

    model.training_state = {'val_loss': [], 'train_loss': [], 'episode': [], 'metrics': []}

    # print('\n\n\n')
    # print(model.training_details['arguments'])
    # print('\n\n\n')

    if CONFIG['CUDA']:
        model.cuda()

    return model
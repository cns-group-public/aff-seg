import os
import cv2
from ava.core.transformations.resize import tensor_resize

if __name__ == '__main__':
    all_ade = list(os.walk('data/ADE20K_2016_07_26'))

    folders = [f for f in os.listdir('data/aff_expert')]

    for f in folders:
        path = [a for a, b, c in all_ade if len(c) > 1 and any([f in r for r in c]) and 'validation' in a][0]

        img = cv2.imread(os.path.join(path, 'ADE_val_' + f + '.jpg'))
        img = tensor_resize(img, (352, 352))
        # print(os.path.join('data/aff_expert/', f, f + '_img.jpg'))
        print('.')
        cv2.imwrite(os.path.join('data/aff_expert/', f, f + '_img.jpg'), img)

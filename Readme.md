# Affordance Segmentation from Single Images

This repository contains code to reproduce the experiments from the paper. This is a simplified version of 
our actual codebase, which is contains only the the necessary modules and should be more comprehensible. We will
assume that all commands are run from the project root (i.e., where `train.py` is located).

## Requirements

### Software
You'll need PyTorch (we use version 0.4) to run the code. 
You can download binaries from [here](https://pytorch.org/). 
Additionally, opencv is required. On Ubuntu there is a package `python3-opencv` which allows
an easy installation. 
```
pip3 install numpy scipy
```

PSPNet uses a dilated ResNet encoder, we rely on the implementation by Fisher Yu:
```
git clone https://github.com/fyu/drn 
```


### Data
We rely on the ADE20K dataset, which is available [here](http://groups.csail.mit.edu/vision/datasets/ADE20K/) and
The Oregon dataset available [here](http://web.engr.oregonstate.edu/~sinisa/research/publications/AffordanceGT.zip).
You can use this script to download both. 
```bash
cd data
wget http://groups.csail.mit.edu/vision/datasets/ADE20K/ADE20K_2016_07_26.zip
unzip ADE20K_2016_07_26.zip -d data/

wget http://web.engr.oregonstate.edu/~sinisa/research/publications/AffordanceGT.zip
unzip AffordanceGT.zip -d data/
```

For legal reasons we can not provide the original images of the expert dataset but only our annotations (aff_expert.zip).
To extract the annotations and transfer the original images run
```bash
unzip data/aff_expert.zip -d data
python3 restore_expert_images.py
```

The simulated data can be obtained with:
```bash
wget -O data/affordances_simulated.zip https://owncloud.gwdg.de/index.php/s/8l3gBSAt1hVgEms/download
unzip data/affordances_simulated.zip -d data/affordances_simulated
```

Now, let's get the pretrained weights:
```bash
wget -O pretrained.zip https://owncloud.gwdg.de/index.php/s/hCbJCtgy3AholZd/download
unzip pretrained.zip
mv out pretrained
```

Make sure the paths in `paths.yaml` are properly set.
* `ADE20K_PATH` should point to a folder containing a folder "ADE20K_2016_07_26".
* `OREGON_AFFORDANCES` should point to a folder containing a folder "Annotations".
* `HQ_AFF` should point to a folder containing 50 folders with number names.
* `SYNTH_AFF`: should point to a folder


## Run the Code
* `train.py [Model] [Dataset]`: Training. You need to provide a model name and a dataset name.
* `score.py [Model] [Dataset]`: Evaluate the network. You need to provide a model name (can be a pretrained file) 
and a dataset name.
* `sample.py [Model] [image filename]`: Visualize predictions by writing an image `output.png`.

`[Model]` can be either `ResNet50Dense` or `PSPNet`
`[Dataset]` can be either `ADEAff12`, `AffHQ12`, `Oregon`, `Oregon-OWN` or `AffSynth12`:


## References

```
@article{luddecke2019context,
  title={Context-based affordance segmentation from 2D images for robot actions},
  author={L{\"u}ddecke, Timo and Kulvicius, Tomas and W{\"o}rg{\"o}tter, Florentin},
  journal={Robotics and Autonomous Systems},
  volume={119},
  pages={92--107},
  year={2019},
  publisher={Elsevier}
}
```


```
@inproceedings{luddecke2017learning,
  title={Learning to segment affordances},
  author={Luddecke, Timo and Worgotter, Florentin},
  booktitle={Proceedings of the IEEE International Conference on Computer Vision Workshops},
  pages={769--776},
  year={2017}
}
```



Have Fun!

import argparse

import time
import torch
from torch.optim import SGD, RMSprop
from torch.utils.data import DataLoader
from ava.core.logging import *
import numpy as np

from common import get_dataset_type, initialize_model, collate, CONFIG


def train(model_name, dataset_name, batch_size=16, epochs=25, decoder_shape='m'):

    dataset_type, dataset_args, other_args = get_dataset_type(dataset_name)
    other_args = {'decoder_shape': decoder_shape}

    d_train = dataset_type('train', None, **dataset_args)
    d_val = dataset_type('val', None, **dataset_args)
    model = initialize_model(model_name, cli_args=other_args, model_config=d_train.model_config)

    loader = DataLoader(d_train, batch_size=batch_size, shuffle=True, num_workers=4, collate_fn=collate)
    loader_val = DataLoader(d_val, batch_size=batch_size, num_workers=4, collate_fn=collate)

    opt = RMSprop([p for p in model.parameters() if p.requires_grad], lr=0.0001)

    print('Train for {} epochs with batch size {} on {} samples'.format(epochs, batch_size, len(d_train)))

    time_start = time.time()


    min_val_loss = 99999
    for i_epoch in range(epochs):
        model.train()
        print('start epoch {}'.format(i_epoch))
        for i, (vars_x, vars_y) in enumerate(loader):

            vars_x = [v.cuda() if v is not None else None for v in vars_x]
            vars_y = [v.cuda() if v is not None else None for v in vars_y]

            opt.zero_grad()
            pred = model(*vars_x)
            loss = model.loss(pred, vars_y)
            loss.backward()
            opt.step()

            if i % 10 == 9:
                print('epoch {} / loss {}'.format(i_epoch, float(loss)))

        print('Epoch done. Start validation...')
        model.eval()
        # validation time
        val_losses = []
        for i, (vars_x, vars_y) in enumerate(loader_val):
            vars_x = [v.cuda() if v is not None else None for v in vars_x]
            vars_y = [v.cuda() if v is not None else None for v in vars_y]

            pred = model(*vars_x)
            val_losses.append(float(model.loss(pred, vars_y)))

        val_loss = np.mean(val_losses)
        if val_loss < min_val_loss:
            print('new best val loss {:.4f}! Saving model parameters'.format(val_loss))
            torch.save({'model':  model_name, 'args': args, 'state_dict': model.state_dict()}, 'model-checkpoint.th')
            min_val_loss = val_loss

    print(time.time() - time_start)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model_name', help='model name')
    parser.add_argument('dataset_name', help='dataset name')
    parser.add_argument('--n', type=int, default=10, help='Number of episodes')
    parser.add_argument('--batch-size', type=int, default=10, help='Batch size')
    parser.add_argument('--precache', default=False, action='store_true')
    parser.add_argument('--decoder-shape', default='m')

    args, unknown_args = parser.parse_known_args()
    train(args.model_name, args.dataset_name, batch_size=args.batch_size)

import inspect
import json
import os
from collections import Counter
from logging import warning

import cv2
import numpy as np
import time
import pickle
import re
from random import seed, shuffle
from scipy.io import loadmat
import yaml
from random import shuffle as random_shuffle
from torch.multiprocessing import Manager

from ava.core.logging import *
from ava.core.transformations.resize import tensor_resize, scale_to_bound
from ava.core.transformations.remap import remap
from ava.core.transformations.color import apply_gamma_offset
from ava.core.transformations.crop import sample_random_patch, random_crop_slices


PATHS = yaml.load(open('paths.yaml', 'r'))

os.path.join(PATHS['AVA_DATA'], 'ade', 'affordances.csv')


def single_parameter_to_str(p):
    if type(p) == bool:
        return 'y' if p else 'n'
    elif type(p) in {list, tuple}:
        return '_'.join(str(a) for a in p)
    elif p is None:
        return 'None'
    else:
        return str(p)


def class_config_str(class_name, parameters):
    param_str = '-'.join(single_parameter_to_str(p) for p in parameters if p is not None)
    return '{}{}'.format(class_name, '-' + param_str if param_str != '' else '')


class Dataset(object):

    # chunk_names = tuple()
    # sample_ids = tuple()
    # slice_labels = None

    def __len__(self):
        raise NotImplementedError

    def generator(self, batch_size, start=0, n_samples=None, shuffle_samples=True, infinite=False):
        raise NotImplementedError


class _DatasetBySample(Dataset):
    """
    Represents a datasets.
    Required attributes:
    - `chunk_names`: a tuple of strings with the same length as the output of `get_sample`.
    - `sample_ids`: list of strings of length of the dataset which names each sample.

    Optional attributes:
    - `chunk_hooks`: can be optionally specified to map the values of specific chunks. E.g. a numeric label
    can be converted to a text label.

    """
    # chunk_names = tuple()
    # chunk_hooks = None
    # sample_ids = tuple()
    # slice_labels = None

    def __init__(self, chunk_names, visualization_modes=None, use_augmentation=False):
        self.chunk_names = chunk_names
        self.visualization_modes = visualization_modes
        self.use_augmentation = use_augmentation
        self.chunk_hooks = dict()
        self.model_config = dict()
        self.parameter_variables = []

    def __getitem__(self, index):
        raise NotImplementedError

    def name(self):
        return class_config_str(self.__class__.__name__, self.parameter_variables)

    def resize(self, n_elements, shuffle=False):

        if shuffle:
            random_shuffle(self.sample_ids)

        self.sample_ids = self.sample_ids[:n_elements]

    def log_important(self, *args):
        log_important(self.__class__.__name__ + ':', *args)

    def log_info(self, *args):
        log_info(self.__class__.__name__ + ':', *args)

    def log_detail(self, *args):
        log_detail(self.__class__.__name__ + ':', *args)

    def __len__(self):
        return len(self.sample_ids)

    def download(self):
        raise NotImplementedError



### CACHE RELATED

_global_cache_manager = Manager()


class DiskCachedFunction(object):
    """ Abstract base class for disk caches. """

    N_THREADS = 8

    def __init__(self, transform, disk_path, cache_size=None, auto_init=True, compression=False):

        self.transform = None
        self.disk_path = os.path.join(PATHS['CACHE'], disk_path)
        self.cache_size = cache_size
        self.compression = compression
        self.storage = {}

        log_info('using cache dir', disk_path)

    def exists(self):
        try:
            transform_code = self.get_stored('transform-code')
            return transform_code is not None
        except:
            return False
        # return os.path.isfile(os.path.join(self.disk_path, '__transform-code__'))

    def set_transform(self, transform):
        if not self.exists():
            self.transform = transform
            self.store('transform-code', inspect.getsource(transform))
            # with open(os.path.join(self.disk_path, '__transform-code__'), 'wb') as f:
            #     pickle.dump(hash(inspect.getsource(transform)), f)
        else:
            self.transform = transform
            transform_code = self.get_stored('transform-code')
            # transform_code = pickle.load(open(os.path.join(self.disk_path, '__transform-code__'), 'rb'))
            if hash(transform_code) != hash(inspect.getsource(transform)):
                warning("""The hash value of the provided transformation function does not match the one
                           stored on disk. This means the transformation has changed and the stored entries
                           are no longer valid. Please recompute them.""")
            # raise ValueError('The transform function can be changed for un-initialized caches only.')

    def add(self, keys, parallel=False):
        """ overwrites """
        t_last_print = time.time()

        if parallel:
            from threading import Thread
            from queue import Queue

            key_queue = Queue()
            threads = list()

            for k in keys:
                key_queue.put(k)

            transform = self.transform
            save_element = self.save_element

            def worker(k):
                x = transform(k)
                save_element(x, k)
                spawn()

            def n_active_threads():
                return sum([1 if t.isAlive() else 0 for t in threads])

            def spawn():
                for _ in range(min(self.N_THREADS - n_active_threads(), key_queue.qsize())):
                    thread = Thread(target=worker, args=(key_queue.get(block=False),))
                    thread.start()
                    threads.append(thread)

            spawn()

            while n_active_threads() > 0:
                time.sleep(0.2)

                if time.time() - t_last_print > 10:
                    print('cache building: ', 100 * len(threads) / key_queue.qsize(), '%')
                    t_last_print = time.time()

            print('completed threads', len(threads))

            for t in threads:
                t.join()

        else:
            for i, key in enumerate(keys):
                if time.time() - t_last_print > 10:
                    print('cache building: ', 100*i / len(keys), '%')
                    t_last_print = time.time()
                self.__call__(key)

    def key_to_str(self, key):
        if type(key) in {tuple, list}:
            return '-'.join([str(x).replace('/', '') for x in key])
        else:
            return str(key).zfill(8).replace('/', '')

    def __call__(self, key):

        # sample_filename = os.path.join(self.disk_path, self.key_to_str(key))

        if self.has_element(key):
            log_detail('serving cached element from disk')
            return self.load_element(key)
        else:
            t_start = time.time()
            x = self.transform(key)
            log_detail('computed key {} (took {}s)'.format(key, time.time() - t_start))

            if self.cache_size is None or len(self.storage) < self.cache_size:
                self.save_element(x, key)

            return x

    def get_stored(self, key):
        raise NotImplementedError

    def store(self, key, value):
        raise NotImplementedError

    def has_element(self, key):
        raise NotImplementedError

    def save_element(self, x, key):
        raise NotImplementedError

    def load_element(self, key):
        raise NotImplementedError


class DiskCachedFunctionFlatFile(DiskCachedFunction):
    """ Abstract base class for flat file disk caches. """

    def __init__(self, transform, disk_path):
        super().__init__(transform, disk_path)

        if not os.path.isdir(self.disk_path):
            os.makedirs(self.disk_path)
            log_important('created new cache')
        else:
            log_important('using disk cache of', len(os.listdir(self.disk_path)), 'elements')

        if transform is not None:
            self.set_transform(transform)

    def store(self, key, value):
        assert type(key) == str
        with open(os.path.join(self.disk_path, self.key_to_str(key)), 'wb') as f:
            pickle.dump(value, f)

    def get_stored(self, key):
        assert type(key) == str
        if os.path.isfile(os.path.join(self.disk_path, self.key_to_str(key))):
            with open(os.path.join(self.disk_path, self.key_to_str(key)), 'rb') as f:
                return pickle.load(f)
        else:
            raise FileNotFoundError('No file corresponding to this key was found')


class DiskCachedFunctionImage(DiskCachedFunctionFlatFile):
    def __init__(self, transform, disk_path, grayscale=False, extra_depth=None, file_ext='.jpg'):
        super().__init__(transform, disk_path)
        import cv2
        self.cv2 = cv2
        self.grayscale = grayscale
        self.extra_depth = extra_depth
        self.file_ext = file_ext  # in tuple mode the first element has a regular name and all others have a postfix number

        assert type(self.file_ext) == str or type(self.file_ext) == tuple and len(self.file_ext) <= 99
        assert type(self.file_ext) == tuple and type(self.grayscale) == tuple or type(self.file_ext) == str and type(self.grayscale) == bool

    def load_element(self, key):
        if type(self.file_ext) == tuple:
            out = []
            for i, ext in enumerate(self.file_ext):
                out += [self._load_image(self.key_to_str(key) + ('_' + str(i).zfill(3) if i != 0 else '') + ext,
                                         self.grayscale[i],
                                         self.extra_depth[i] if self.extra_depth is not None else None)]
            return tuple(out)
        else:
            return self._load_image(self.key_to_str(key) + self.file_ext, self.grayscale,
                                    self.extra_depth if self.extra_depth is not None else None)

    def _load_image(self, filename, grayscale, extra_depth):
        sample_filename = os.path.join(self.disk_path, filename)
        if os.stat(sample_filename).st_size > 0:
            flags = self.cv2.IMREAD_GRAYSCALE if grayscale else self.cv2.IMREAD_COLOR
            if extra_depth:
                flags += self.cv2.IMREAD_ANYDEPTH
            img = self.cv2.imread(sample_filename, flags)
            assert img is not None

            if img.ndim == 3:
                img = self.cv2.cvtColor(img, self.cv2.COLOR_BGR2RGB)

            return img
        else:
            return None

    def save_element(self, x, key):
        if type(self.file_ext) == tuple:
            for i, ext in enumerate(self.file_ext):
                self._save_image(x[i], self.key_to_str(key) + ('_' + str(i).zfill(3) if i != 0 else '') + ext,
                                 self.grayscale[i])
        else:
            self._save_image(x, self.key_to_str(key) + self.file_ext, self.grayscale)

    def _save_image(self, img, filename, grayscale):
        sample_filename = os.path.join(self.disk_path, filename)
        if img is not None:
            assert img.ndim == 2 and grayscale or img.ndim == 3 and not grayscale

            if img.ndim == 3:
                img = self.cv2.cvtColor(img, self.cv2.COLOR_RGB2BGR)

            self.cv2.imwrite(sample_filename, img)
        else:
            with open(sample_filename, 'wb') as f:
                pass

    def has_element(self, key):
        sample_filename = os.path.join(self.disk_path, self.key_to_str(key) + (self.file_ext if type(self.file_ext) == str else self.file_ext[0]))
        return os.path.isfile(sample_filename)


class MemoryCachedFunctionBase(object):

    def __init__(self, transform, cache_size=None):
        self.transform = transform
        self.cache_size = cache_size
        self.storage = {}
        self.last_info_time = 0
        self.info_interval = 90  # in seconds


class MemoryCachedFunction(MemoryCachedFunctionBase):

    def __init__(self, transform, cache_size=None):
        super().__init__(transform, cache_size)
        self.storage = _global_cache_manager.dict()
        log_info('Memory cache initialized: Size', 'Infinite' if cache_size is None else cache_size)

    def __call__(self, key):
        if key in self.storage:
            item = self.storage[key]
            # print('found in cache', key, 'size', len(self.storage), 'time', time.time() - t_start)
            return item
        else:
            x = self.transform(key)

            if self.cache_size is None or len(self.storage) < self.cache_size:
                self.storage[key] = x
                if time.time() - self.last_info_time > self.info_interval:
                    print('cache: ', len(self.storage), 'elements')
                    self.last_info_time = time.time()

            return x


def load_property_map(filename, add_bg_col_zero=False, second_row_weights=False, delimiter=','):
    """
    Parse a property remapping csv file according to this scheme:
    1 means known presence of a property
    0 means known absence of a property
    An empty cell means uncertainty about the presence of the property (hence mask will be zero).
    """
    object_to_property, object_to_mask = dict(), dict()
    zero_col = [0.0] if add_bg_col_zero else []
    weights = None

    with open(filename, 'r') as f:
        lines = f.read().split('\n')
        properties = [p.strip() for p in lines[0].split(delimiter)[2:]]

        if second_row_weights:
            cols = lines[1].split(delimiter)
            weights = ([1.0] if add_bg_col_zero else []) + [float(c) if c != '' else float(0) for c in cols[2:]]

        for line in lines[1:]:
            cols = line.split(delimiter)
            if len(cols[2:]) == len(properties):
                object_to_property[cols[0].strip()] = zero_col + [float(c) if c != '' else float(0) for c in cols[2:]]
                object_to_mask[cols[0].strip()] = zero_col + [1 if c != '' else 0 for c in cols[2:]]

    if add_bg_col_zero:
        properties = ['bg'] + properties

    return object_to_property, object_to_mask, properties, weights


def get_subset_names(subset):
    if subset == 'train':
        subsets = 'training', 'train'
    elif subset == 'val':
        subsets = 'validation', 'val'
    elif subset == 'test':
        subsets = 'test', 'test'
    else:
        raise ValueError('invalid subset: ' + str(subset))
    return subsets


class ADE(_DatasetBySample):
    """
    Generic ADE dataset.
    `important_sampling_labels` Provide a list or filename that contains labels which are used to sample crops
    `remap` Filename that contains a csv file which maps labels from the original ADE names to a new space.
    `with_coverage_mask` Use first map to indicate coverage
    `no_exclusive_labels` A pixel can have more than one active map
    """

    def __init__(self, subset, max_samples, remap=None, importance_sampling_labels=None,
                 importance_sampling_offset=0.0, augmentation=False,
                 no_exclusive_labels=False, remap_with_coverage_mask=False, hints=False, image_size=352, cache=False,
                 select_scenes=False):

        super().__init__(
            ('image', 'segmentation', 'mask'),  # 'seg_parts', 'importance'),
            visualization_modes=('Image', 'ImageLabel', None))  # 'ImageLabel', 'HeatMap'))

        # self.tmp_class = np.array([False for _ in range(14)])
        self.augmentation = augmentation
        # self.importance_sampling = importance_sampling
        self.subsets = get_subset_names(subset)

        self.cache = cache
        self.remap = remap
        self.no_exclusive_labels = no_exclusive_labels
        self.importance_sampling_labels = importance_sampling_labels
        self.importance_sampling_offset = importance_sampling_offset  # probability of non-matching pixels
        self.select_scenes = select_scenes  # select scenes
        self.remap_with_coverage_mask = remap_with_coverage_mask
        self.target_size = image_size

        # self.intermediate_size_range = (370, 550)
        self.intermediate_size_range = (image_size, 750)
        # self.intermediate_size_range = (750, 850, 550)
        self.root_path = PATHS['ADE20K_PATH']
        self.data_root = os.path.join(PATHS['AVA_DATA'], 'ade')
        # scene_names = get_ade20k_scene_names(self.root_path, limit=max_samples)
        # self.sample_ids = tuple(scene_names[:max_samples])

        with open(os.path.join(self.data_root, 'scene_folders.txt'), 'r') as fh:
            self.scene_paths = fh.read().split('\n')[:-1]

        assert len(self.scene_paths) == 22210

        # import object info from objects.txt
        with open(os.path.join(self.data_root, 'objects.txt'), 'r') as fh:
            objects = [a.split(';') for a in fh.read().split('\n')]

        objects = [(name, int(count), float(ispart)) for name, count, ispart in objects]
        self.objects, self.object_counts, self.object_ispart = zip(*objects)

        # use the abbreviated object names
        # self.objects = [k[:k.index(',')] if ',' in k else k for k in self.objects]
        # print(len(self.objects), self.objects[:10])

        duplicates = [o for o, c in Counter(self.objects).most_common() if c > 1]
        assert len(self.objects) == len(set(self.objects)), 'There are duplicates in objects: ' + ', '.join(duplicates)

        # self.object_counts = [int(c) for c in open('ava/data/ade/object_counts.txt', 'r').read().split('\n')]
        self.object_index = {o: i for i, o in enumerate(self.objects)}

        scene_objects = np.load(os.path.join(self.data_root, 'object_scene_occurrences.npz'))['arr_0']

        if self.subsets[1] == 'train':
            self.subset_path = 'train'
            self.offset = 0
            self.sample_ids = tuple(('train', i) for i in range(1, 19211))  # one-based counting for the scene ids
            # self.scene_paths = self.scene_paths[19210: 20210]
            scene_objects = scene_objects[0: 19210]
        elif self.subsets[1] == 'val':
            self.subset_path = 'train'
            self.offset = 0
            self.sample_ids = tuple(('train', i) for i in range(19211, 20211))  # one-based counting for the scene ids
            # self.scene_paths = self.scene_paths[19210: 20210]
            scene_objects = scene_objects[19210: 20210]
        elif self.subsets[1] == 'test':
            self.subset_path = 'val'
            self.offset = 20210
            self.sample_ids = tuple(('val', i) for i in range(1, 2001))  # self.scene_objects[20210: 22210]
            # self.scene_paths = self.scene_paths[20210: 22210]
            scene_objects = scene_objects[20210: 22210]
        else:
            raise ValueError('invalid subset')

        # self.sample_ids = tuple(self.scene_numbers)

        # valid_scene_ids = [i for i in range(len(self.sample_ids)) if self.scenes[i] != '~not labeled']
        # self.sample_ids = tuple(self.sample_ids[i] for i in valid_scene_ids)
        # self.scenes = [self.scenes[i] for i in valid_scene_ids]
        # scene_objects = scene_objects[valid_scene_ids]

        if self.importance_sampling_labels is not None:

            if type(self.importance_sampling_labels) in {list, tuple}:
                self.desired_objects = self.importance_sampling_labels
            elif type(self.importance_sampling_labels) == str:
                with open(self.importance_sampling_labels, 'r') as fh:
                    self.desired_objects = [o for o in fh.read().split('\n') if len(o) > 0]
            else:
                raise ValueError('Invalid type of important_sampling_labels:',
                                 type(self.importance_sampling_labels).__name__)

            print('desired', self.desired_objects)
            unknown_objects = [o for o in self.desired_objects if o not in self.object_index]
            msg = 'Some of the importance sampling objects are unknown: {}'.format('\n'.join(unknown_objects))
            assert len(unknown_objects) == 0, msg

            self.desired_indices = [self.object_index[o] for o in self.desired_objects]

            n_desired_objects = scene_objects[:, [i for i in self.desired_indices]].sum(1)

            assert len(n_desired_objects) == len(self.sample_ids)

            if self.select_scenes:
                selected_scene_indices = np.argwhere(n_desired_objects > 0)
                selected_scene_indices = selected_scene_indices[:, 0]
                self.sample_ids = tuple(self.sample_ids[i] for i in selected_scene_indices)

            # self.scenes = tuple(self.scenes[i] for i in selected_scene_indices)
            log_important('Using importance sampling. Number of remaining scenes', len(self.scene_paths))

        if self.remap is not None:
            log_important('ADE: Using remapping from file:', remap)

            property_map = load_property_map(remap, add_bg_col_zero=not self.no_exclusive_labels,
                                             second_row_weights=True, delimiter=';')
            label_remapping_in, label_remapping_mask_in, self.properties, self.property_weights = property_map

            label_remapping_in = {k: v for k, v in label_remapping_in.items() if not k.startswith('_')}
            assert all([w in {0, 1} for v in label_remapping_in.values() for w in v])

            # change visualization mode if required
            vis_modes = list(self.visualization_modes)
            if self.no_exclusive_labels:
                vis_modes[1] = ('Slices', {'maps_dim': 0, 'slice_labels': self.properties})

                if self.remap_with_coverage_mask:
                    vis_modes[2] = ('Slices', {'maps_dim': 0, 'slice_labels': self.properties})
            else:
                if self.remap_with_coverage_mask:
                    vis_modes[2] = ('Image', {'channel_dim': None})

            self.visualization_modes = tuple(vis_modes)

            self.label_remapping, self.label_remapping_mask = dict(), dict()
            fail_due_to_unknown_object = False
            # add hierarchical labels
            # * is not necessary because it equivalent to specifying affordances for the part name directly
            for k in label_remapping_in.keys():

                if k.startswith('_'):  # ignore this key
                    pass
                elif '/' in k:  # assume hierarchical item
                    k_tuple = []
                    for key in k.split('/'):
                        if key in self.object_index:
                            k_tuple += [self.object_index[key]]
                        elif key == '*':
                            k_tuple += ['*']
                        else:
                            log_important('object "{}" was not found in object_indices'.format(key))
                            fail_due_to_unknown_object = True
                    # assert k.count('/') == 1, '{}: currently only hierarchies of depth 1 are supported.'.format(k)
                    # k_tuple = tuple(self.object_index[key] for key in k.split('/'))
                    # object_id = self.object_index[object_name] if object_name != '*' else None
                    # part_id = self.object_index[part_name]
                    self.label_remapping[tuple(k_tuple)] = label_remapping_in[k]
                    self.label_remapping_mask[tuple(k_tuple)] = label_remapping_mask_in[k]
                else:
                    if k in self.object_index:
                        self.label_remapping[self.object_index[k]] = label_remapping_in[k]
                        self.label_remapping_mask[self.object_index[k]] = label_remapping_mask_in[k]
                    else:
                        log_important('object "{}" was not found in object_indices'.format(k))
                        fail_due_to_unknown_object = True

            if not self.remap_with_coverage_mask:
                self.label_remapping_mask = None

            if fail_due_to_unknown_object:
                raise ValueError('Aborted due to unknown objects in the remap labels.')
            #
            # self.label_remapping = {self.object_index[k]: v for k, v in self.label_remapping.items()
            #                         if k in self.object_index}

            if hints:
                sorted_objects = sorted(list(zip(self.object_counts, self.objects)), key=lambda x: int(x[0]),
                                        reverse=True)
                missing_objects = [o for c, o in sorted_objects if self.object_index[o] not in self.label_remapping and
                                   '_' + o not in label_remapping_in][:50]
                log_info('100 frequent, but missing object classes', '\n'.join(missing_objects))

                part_freqs = [float(self.object_counts[i]) * float(self.object_ispart[i]) for i in
                              range(len(self.objects))]
                missing_part_indices = np.argsort(part_freqs)[::-1][:50]

                log_info('100 frequent, but missing part classes (hierarchical labels not included)',
                         '\n'.join(self.objects[i] for i in missing_part_indices
                                   if i not in self.label_remapping and
                                   all([k[-1] != i for k in self.label_remapping.keys() if type(k) == tuple])))

            self.model_config.update({
                'out_channels': len(self.properties),
            })

        # limit the number of samples
        if max_samples is not None:
            self.sample_ids = self.sample_ids[:max_samples]

        self.downsample_ade_image = DiskCachedFunctionImage(self.downsample_ade_image, 'ade20k_images', file_ext='.jpg')

        if self.cache:
            log_important('Using in memory cache. Be careful if you do not have a lot of RAM.')
            self.load_and_remap_ade_parts = MemoryCachedFunction(self.load_and_remap_ade_parts)

    def name(self):
        return '{}-{}{}{}'.format(self.__class__.__name__, self.target_size,
                                  'imp' + str(
                                      self.importance_sampling_offset) if self.importance_sampling_labels is not None else '',
                                  'sel' if self.select_scenes else '')

    def downsample_ade_image(self, index):
        index, subset_path = index
        path = self.scene_paths[index + self.offset - 1]
        sample_num = str(index).zfill(8)

        img_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '.jpg')
        img = cv2.imread(img_path)
        assert img is not None, 'image not found: {}'.format(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = tensor_resize(img, (self.intermediate_size_range[1],) * 2, interpret_as_min_bound=True)
        return img

    def load_ade_parts(self, index):

        index, subset_path = index
        path = self.scene_paths[index + self.offset - 1]
        sample_num = str(index).zfill(8)

        interm_size = (self.intermediate_size_range[1],) * 2
        seg_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_seg.png')
        part1_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_parts_1.png')
        part2_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_parts_2.png')
        part3_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_parts_3.png')

        seg = cv2.imread(seg_path)
        seg = tensor_resize(seg, interm_size, interpret_as_min_bound=True, interpolation='nearest')
        seg = seg.astype('int16')
        seg = np.maximum(0, (seg[:, :, 2] // 10) * 256 + seg[:, :, 1] - 1).astype('uint16')  # convert to label ids

        part_segs = []
        for p in [part1_path, part2_path, part3_path]:
            if os.path.isfile(p):
                part_seg = cv2.imread(p)
                if part_seg is not None:
                    part_seg = tensor_resize(part_seg, interm_size, interpret_as_min_bound=True,
                                             interpolation='nearest')

                part_seg = part_seg.astype('int16')
                part_seg = np.maximum(0, (part_seg[:, :, 2] // 10) * 256 + part_seg[:, :, 1] - 1).astype('uint16')
                part_segs += [part_seg]
            else:
                part_segs += [None]

        return seg, part_segs[0], part_segs[1], part_segs[2]

    def load_and_remap_ade_parts(self, index):
        # seg, part0, part1, part2 = self.intermediate_part_cache(index)
        seg, part0, part1, part2 = self.load_ade_parts(index)
        part_seg = [part0, part1, part2]

        occurring_objects = np.argwhere(np.bincount(seg.flatten()) > 0)[:, 0]
        # print('occurring objects:', '  |  '.join(self.objects[j] for j in occurring_objects))

        occurring_parts = []
        for i in range(len(part_seg)):
            if part_seg[i] is not None:
                occurring_parts += [np.argwhere(np.bincount(part_seg[i].flatten()) > 0)[:, 0]]
            else:
                occurring_parts += [[]]

        # print('occurring parts 0:', '  |  '.join(self.objects[j] for j in occurring_parts[0]))
        # print('occurring parts 1:', '  |  '.join(self.objects[j] for j in occurring_parts[1]))

        if self.importance_sampling_labels is not None:

            importance_map = [(seg == i).astype('uint8') for i in self.desired_indices if i in occurring_objects]
            for l in range(3):
                if part_seg[l] is not None:
                    importance_map += [(part_seg[l] == i).astype('uint8') for i in self.desired_indices
                                       if i in occurring_parts[l]]

            if len(importance_map) > 0:
                importance_map = np.dstack(importance_map).sum(2).astype('float32')
                importance_map = np.clip(importance_map, 0, 1)
                importance_map = self.importance_sampling_offset + (
                            1 - self.importance_sampling_offset) * importance_map
            else:
                importance_map = None
                # log_warning('No importance sampling labels found in the actual maps '
                #             '(this could be due to strong down-sizing).')
        else:
            importance_map = None

        seg_shape = seg.shape

        mask = None
        if self.remap:
            seg, mask = remap([seg] + part_seg, [occurring_objects] + occurring_parts,
                              self.label_remapping, self.label_remapping_mask, self.no_exclusive_labels)

        assert seg.shape[:2] == seg_shape[:2], '{}  vs. {}'.format(seg.shape[:2], seg_shape[:2])
        # seg = seg > 0.5
        # print(describe(seg))
        # print(describe(importance_map))
        # print(describe(mask))

        if seg.ndim == 3 and self.cache:
            seg = seg.astype('bool')
            seg = seg.shape, np.packbits(seg)
            mask = mask.shape, np.packbits(mask)

        # return np.array([seg, mask, importance_map])
        return seg, mask, importance_map
        # return seg, importance_map, mask

    # @profile
    def __getitem__(self, index):

        subset_path, real_index = self.sample_ids[index]

        # print('index | real_index | sample_ids', index, real_index, self.sample_ids[index])


        img = self.downsample_ade_image((real_index, subset_path))
        seg, mask, importance_map = self.load_and_remap_ade_parts((real_index, subset_path))

        if type(seg) == tuple:
            seg = np.unpackbits(seg[1]).reshape(seg[0]).astype('float32')

        if type(mask) == tuple:
            mask = np.unpackbits(mask[1]).reshape(mask[0]).astype('float32')
        # importance_map = Noney

        if not self.augmentation:

            if self.target_size is not None:
                seg = seg.astype('float32')
                img = tensor_resize(img, (self.target_size,) * 2)
                seg = tensor_resize(seg, (self.target_size,) * 2, interpolation='nearest')
                if mask is not None:
                    mask = mask.astype('float32')
                    mask = tensor_resize(mask, (self.target_size,) * 2, interpolation='nearest')

            img = img.transpose([2, 0, 1]).astype('float32')

            if seg.ndim == 3:
                seg = seg.transpose([2, 0, 1])
                seg = seg.astype('float32')
            else:
                seg = seg.astype('int64')

            if mask is not None:
                mask = mask.transpose([2, 0, 1]).astype('float32')

            return (img,), (seg, mask,)
            #
            #         if self.remap_with_coverage_mask:
            #             seg_crop_mask[matching_indices] = self.label_remapping_mask[(a, b)]

        else:
            if seg.dtype == 'bool' and self.cache:
                seg = seg.astype(
                    'float32')  # this is computationally expensive but saves memory, which is useful for caching

            intermediate_size = np.random.uniform(self.intermediate_size_range[0], self.intermediate_size_range[1])
            intermediate_size = int(
                np.clip(intermediate_size, max(intermediate_size - 150, self.target_size), intermediate_size + 150))
            intermediate_size = (intermediate_size, intermediate_size)

            # print(1, img.shape, seg.shape, importance_map.shape if importance_map is not None else None, mask.shape if mask is not None else None)
            # print(intermediate_size)

            # Depending on input size, the output of scale_to_bound might vary by a pixel. Therefore, do it only once.
            intermediate_size = scale_to_bound(img.shape, intermediate_size, interpret_as_max_bound=False)

            img = tensor_resize(img, intermediate_size)
            seg = tensor_resize(seg, intermediate_size, interpolation='nearest')

            assert img.shape[:2] == seg.shape[:2], '{} vs. {}'.format(img.shape, seg.shape)

            if mask is not None:
                # print(mask.shape)
                mask = tensor_resize(mask.astype('float32'), intermediate_size, interpret_as_min_bound=True,
                                     interpolation='nearest', channel_dim=2)

            # print(2, img.shape, seg.shape, importance_map.shape, mask.shape if mask is not None else None)

            if importance_map is not None:
                # importance_map = tensor_resize(importance_map, intermediate_size, interpret_as_min_bound=True)
                assert importance_map.sum() > 0
                slice_indices = sample_random_patch(img.shape, (self.target_size, self.target_size), importance_map)
            else:
                slice_indices = random_crop_slices(img.shape, (self.target_size, self.target_size))

            assert img.shape[:2] == img.shape[:2]

            seg_crop = seg[slice_indices]
            img_crop = img[slice_indices]
            mask_crop = mask[slice_indices] if mask is not None else None
            img_crop = apply_gamma_offset(img_crop, gamma=np.random.normal(1, 0.08, 3),
                                          offset=np.random.randint(-5, 5, 3))

            assert seg_crop.shape[0] == self.target_size, seg_crop.shape
            assert seg_crop.shape[1] == self.target_size, seg_crop.shape

            # import ipdb
            # ipdb.set_trace()

            if np.random.rand() > 0.5:  # random horizontal flip
                img_crop = img_crop[:, ::-1]
                seg_crop = seg_crop[:, ::-1]
                if mask_crop is not None:
                    mask_crop = mask_crop[:, ::-1]

            # channels first
            img_crop = img_crop.transpose([2, 0, 1])

            if self.no_exclusive_labels:
                seg_crop = seg_crop.transpose([2, 0, 1])

                if mask_crop is not None:
                    mask_crop = mask_crop.transpose([2, 0, 1])
                    mask_crop = mask_crop

            img_crop = img_crop.astype('float32')

            if seg_crop.dtype.name in {'float64', 'uint8'}:
                seg_crop = seg_crop.astype('float32')

            if seg_crop.dtype.name == 'uint16':
                seg_crop = seg_crop.astype('int64')

            return (img_crop,), (seg_crop, mask_crop)




class ADEAff12(ADE):
    def __init__(self, subset, max_samples, hints=False, augmentation=False, image_size=352, cache=False,
                 importance_sampling_labels=None, select_scenes=False):
        super().__init__(subset, max_samples, augmentation=augmentation, cache=cache,
                         remap=os.path.join(PATHS['AVA_DATA'], 'ade', 'affordances12.csv'),
                         importance_sampling_labels=importance_sampling_labels,
                         importance_sampling_offset=0.1,
                         select_scenes=select_scenes,
                         no_exclusive_labels=True,
                         remap_with_coverage_mask=True,
                         image_size=image_size,
                         hints=hints)
        self.model_config['binary'] = True


class AffHQ12(_DatasetBySample):

    def __init__(self, subset, max_samples, augmentation=False, cache=False):
        filename = os.path.join(PATHS['AVA_DATA'], 'ade', 'affordances12.csv')
        _, _, self.parts, _ = load_property_map(filename, add_bg_col_zero=False, second_row_weights=True, delimiter=';')
        super().__init__(('image', 'label', 'mask'),
                         visualization_modes=('Image', ('Slices', {'maps_dim': 0, 'slice_labels': self.parts}), None))

        def threshold(x):
            x = x.astype('float32')
            return np.uint8(np.maximum(0, x[:, :, 2] - x[:, :, 0] - x[:, :, 1]) > 20)

        self.threshold = threshold
        self.parts = [p.replace('/', '_').replace('clean_dry', 'dry') for p in self.parts]

        self.image_fp = lambda folder: '{}/{}_img.jpg'.format(folder, folder)
        self.part_fp = lambda folder, part: '{}/{}_{}.png'.format(folder, folder, part)

        self.path = PATHS['HQ_AFF']
        self.sample_ids = tuple(d for d in os.listdir(self.path) if os.path.isdir(os.path.join(self.path, d)))
        self.sample_ids = self.sample_ids[:max_samples]
        self.model_config.update({'binary': True, 'with_mask': True, 'out_channels': len(self.parts)})

    def __getitem__(self, index):

        img = cv2.imread(os.path.join(self.path, self.image_fp(self.sample_ids[index])))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        seg = []
        for part in self.parts:
            if part == 'pull':
                filename = os.path.join(self.path, self.part_fp(self.sample_ids[index], 'hook_pull'))
                prop_img1 = cv2.imread(os.path.join(self.path, self.part_fp(self.sample_ids[index], 'hook_pull')))
                prop_img2 = cv2.imread(os.path.join(self.path, self.part_fp(self.sample_ids[index], 'pinch_pull')))
                seg_img = cv2.add(prop_img1, prop_img2)
                assert seg_img is not None, 'image not found for {}'.format(filename)
            else:
                filename = os.path.join(self.path, self.part_fp(self.sample_ids[index], part))
                seg_img = cv2.imread(filename)
                assert seg_img is not None, 'image not found: {}'.format(filename)

            seg += [np.uint8(seg_img[:, :, 0] > 0)]

        seg = np.array(seg, dtype='float32')
        # seg = seg.argmax(0)
        # mask = None
        # if self.mask_fp is not None:
        #     mask = cv2.imread(os.path.join(self.path, self.mask_fp(self.sample_ids[index])), cv2.IMREAD_GRAYSCALE)

        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        return (img,), (seg, np.ones(seg.shape, dtype='float32'))



class AffSynth(_DatasetBySample):

    def __init__(self, subset, max_samples, augmentation=False, without_ground_truth=False,
                 filename=os.path.join(PATHS['AVA_DATA'], 'ade', 'affordances.csv')):

        self.augmentation = augmentation
        _, _, properties, _ = load_property_map(filename, add_bg_col_zero=False, second_row_weights=True, delimiter=';')

        super().__init__(
            ('image', 'segmentation', 'mask'),
            visualization_modes=('Image',
                                 ('Slices', dict(maps_dim=0, slice_labels=properties)),
                                 ('Slices', dict(maps_dim=0, slice_labels=properties)))
        )

        self.properties = [p for p in properties]
        self.property_filenames = [p.replace('clean_dry', 'dry') for p in self.properties]
        self.property_filenames = [p.replace('read/watch', 'observe') for p in self.property_filenames]
        self.property_filenames = [p.replace('_', '-') for p in self.property_filenames]

        # use training data for validation
        self.path = os.path.join(PATHS['SYNTH_AFF'], subset if subset != 'val' else 'train')

        prefixes = set()
        for p in os.listdir(self.path):
            filenames = os.listdir(os.path.join(self.path, p))
            for filename in filenames:
                m = re.match(r'^([0-9]*)_(.*)\.png$', filename)
                if m:
                    prefixes.add(os.path.join(p, m.group(1)))

        prefixes = sorted(prefixes)

        if subset == 'train':
            prefixes = [p for p in prefixes if not p.startswith('b15') and not p.startswith('b14')]
        elif subset == 'val':
            prefixes = [p for p in prefixes if p.startswith('b15') or p.startswith('b14')]
        else:
            pass  # for test nothing needs to be done because we scanned a different path

        prefixes = prefixes[:max_samples]
        self.sample_ids = tuple(prefixes)

        self.model_config['binary'] = True
        self.model_config['with_mask'] = True
        self.model_config['out_channels'] = len(properties)

    def __getitem__(self, index):
        prefix = self.sample_ids[index]
        img_path = os.path.join(self.path, prefix + '_rgb.png')
        img = cv2.imread(img_path)
        img = tensor_resize(img, (352, 352))

        properties = []
        for prop in self.property_filenames:
            property_path = os.path.join(self.path, '{}_{}.png'.format(prefix, prop))

            # hack to deal with Aff12 which contains only pull
            if prop == 'pull':
                prop_img1 = cv2.imread(os.path.join(self.path, '{}_{}.png'.format(prefix, 'pinch-pull')), cv2.IMREAD_GRAYSCALE)
                prop_img2 = cv2.imread(os.path.join(self.path, '{}_{}.png'.format(prefix, 'hook-pull')), cv2.IMREAD_GRAYSCALE)
                prop_img = cv2.add(prop_img1, prop_img2)
            else:
                prop_img = cv2.imread(property_path, cv2.IMREAD_GRAYSCALE)

            prop_img = tensor_resize(prop_img, (352, 352)) / 255.0
            properties += [prop_img]

        # make sure affordances are binary
        properties = (np.array(properties) > 0.5).astype('float32')

        if self.augmentation:
            img = apply_gamma_offset(img, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))

        img = img.transpose([2, 0, 1])
        # properties = properties.transpose([2, 0, 1])
        mask = np.ones_like(properties)
        #
        # print(describe(img))
        # print(describe(properties))

        return (img.astype('float32'),), (properties, mask,)


class AffSynth12(AffSynth):

    def __init__(self, subset, max_samples, augmentation=False, cache=False):
        super().__init__(subset, max_samples, augmentation=augmentation,
                         filename=os.path.join(PATHS['AVA_DATA'], 'ade', 'affordances12.csv'))


class Interleaved(_DatasetBySample):
    """
    Merge two datasets such that samples are interleaved. Without shuffling they will simply be after each other
    """

    def __init__(self, subset, max_samples, dataset1, dataset2, visualization_modes=None, model_config=None,
                 strict=True):
        super().__init__(dataset1.chunk_names, dataset1.visualization_modes)
        print('q')

        self.dataset1 = dataset1
        self.dataset2 = dataset2

        len1 = len(dataset1)
        len2 = len(dataset2)

        # indices1 = np.floor(np.arange(0, len1 + len2, (1 + len2 / len1))).astype('int')

        indices1 = (np.linspace(0, 1, len1)*(len1 + len2 - 1)).astype('int')

        self.from1 = np.zeros(len1 + len2, dtype='bool')
        self.from1[indices1] = True

        assert np.sum(self.from1) == len(dataset1)

        self.sample_ids, i1, i2 = [], 0, 0
        self.rel_index = []  # the actual indices inside the datasets
        for f in self.from1:
            if f:
                self.sample_ids += [self.dataset1.sample_ids[i1]]
                self.rel_index += [i1]
                i1 += 1
            else:
                self.sample_ids += [self.dataset2.sample_ids[i2]]
                self.rel_index += [i2]
                i2 += 1

        self.sample_ids = tuple(self.sample_ids)
        self.rel_index = tuple(self.rel_index)
        assert len(self.sample_ids) == len1 + len2

        if model_config is None:
            self.model_config = dataset1.model_config
        else:
            self.model_config = model_config

        self.sample_ids = tuple(self.sample_ids)
        self.rel_index = tuple(self.rel_index)
        assert len(self.sample_ids) == len1 + len2

    def __getitem__(self, index):
        dataset = self.dataset1 if self.from1[index] else self.dataset2
        return dataset[self.rel_index[index]]


class ADEAffSynth12(Interleaved):

    def __init__(self, subset, max_samples, augmentation=False, cache=False):

        max_samples = max_samples // 2 if max_samples is not None else None

        dataset1 = ADEAff12(subset, max_samples, augmentation=augmentation, cache=cache)
        dataset2 = AffSynth12(subset, max_samples, augmentation=augmentation)

        model_config = dataset1.model_config

        super().__init__(subset, max_samples, dataset1, dataset2, model_config=model_config)


OREGON_AFFORDANCE_PROPERTIES = ['walkable', 'sittable', 'lyable', 'reachable', 'movable']


class OregonAffordances(_DatasetBySample):

    def __init__(self, subset, max_samples, augmentation=False, split='nyu'):
        super().__init__(('image', 'affordance', 'mask'),
                         visualization_modes=('Image',
                                              ('Slices', {'maps_dim': 0, 'slice_labels': OREGON_AFFORDANCE_PROPERTIES}), None),
                         use_augmentation=augmentation)

        self.root = PATHS['OREGON_AFFORDANCES']
        self.target_size = 352
        self.intermediate_size_range = (370, 550)

        self.properties = OREGON_AFFORDANCE_PROPERTIES

        if split == 'nyu':  # ORIGINAL NYUv2 splits
            splits = loadmat(os.path.join(PATHS['AVA_DATA'], 'nyu_splits.mat'))

            def map_sample_id(ids):
                return ['5' + str(i[0]).zfill(4) for i in ids]

            self.sample_ids = {
                'train': map_sample_id(splits['trainNdxs'][:600]),
                'val': map_sample_id(splits['trainNdxs'][600:]),
                'test': map_sample_id(splits['testNdxs'])}[subset]

        elif split == 'own':
            # MORE-TRAINING-DATA SPLIT

            sample_ids = json.load(open(os.path.join(PATHS['AVA_DATA'], 'sample_id_oregon.json')))
            seed(12345)
            shuffle(sample_ids)

            self.sample_ids = {
                'train': sample_ids[:1000],
                'val': sample_ids[1000:1100],
                'test': sample_ids[1100:]}[subset]
        else:
            raise ValueError('Invalid splits')

        if max_samples:
            self.sample_ids = self.sample_ids[:max_samples]

        self.sample_ids = tuple(self.sample_ids)

        self.model_config.update({'with_mask': False, 'out_channels': len(self.properties), 'binary': True})
        self.parameter_variables = [split, self.target_size]

    def load_img(self, filename):
        img = cv2.imread(filename)
        return img

    def __getitem__(self, index):

        index = self.sample_ids[index]
        sample_path = os.path.join(self.root, 'Annotations')

        img = self.load_img(sample_path + '/{}/{}.png'.format(index, index))
        assert img is not None, sample_path + '/{}/{}.png'.format(index, index)

        if self.use_augmentation:
            intermediate_size = int(np.random.uniform(self.intermediate_size_range[0], self.intermediate_size_range[1]))
            intermediate_size = intermediate_size, intermediate_size
        else:
            intermediate_size = img.shape[:2]

        img = tensor_resize(img, intermediate_size, interpret_as_min_bound=True, interpolation='bilinear',
                            channel_dim=2)

        maps = []
        for p in self.properties:
            # print(sample_path + '/{}_{}.png'.format(index, p[0].upper()))
            map = cv2.imread(sample_path + '/{}/{}_{}.png'.format(index, index, p[0].upper()), cv2.IMREAD_GRAYSCALE)
            map = tensor_resize(map, intermediate_size, interpret_as_min_bound=True, interpolation='nearest')
            maps += [map]

        maps = np.array(maps, dtype='float32') / 255.0
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        if self.use_augmentation:
            slices = random_crop_slices(img.shape[:2], (self.target_size, self.target_size))
            img = img[slices + (slice(0, None),)]
            maps = maps[(slice(0, None),) + slices]

        # print(maps.shape, maps.dtype, img.shape, img.dtype)
        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        return (img,), (maps, None)
